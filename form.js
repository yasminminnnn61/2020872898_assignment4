/* 
        Name: NUR YASMIN BINTI MOHD KAHAR
        Date: 20/6/2021
*/
function score() {
    var question1 = document.forms["quiz"]["question1"].value;
    var question2 = document.forms["quiz"]["question2"].value;
    var question3 = document.forms["quiz"]["question3"].value;
    var question4 = document.forms["quiz"]["question4"].value;
    var question5 = document.forms["quiz"]["question5"].value;
    var question6 = document.forms["quiz"]["question6"].value;
    var question7 = document.forms["quiz"]["question7"].value;
    var question8 = document.forms["quiz"]["question8"].value;
    var question9 = document.forms["quiz"]["question9"].value;
    var question10 = document.forms["quiz"]["question10"].value;
    var name = document.forms["quiz"]["name"].value;

    var score = 0;
    
    if (question1 == "") {
        alert("Oops!!Question 1 is required");
        return false;
    }
    else if (question2 == "") {
        alert("Oops!!Question 2 is required");
        return false;
    }
    else if (question3 == "") {
        alert("Oops!!Question 3 is required");
        return false;
    }
    else if (question4 == "") {
        alert("Oops!!Question 4 is required");
        return false;
    }
    else if (question5 == "") {
        alert("Oops!!Question 5 is required");
        return false;
    }
    else if (question6 == "") {
        alert("Oops!!Question 6 is required");
        return false;
    }
    else if (question7 == "") {
        alert("Oops!!Question 7 is required");
        return false;
    }
    else if (question8 == "") {
        alert("Oops!!Question 8 is required");
        return false;
    }
    else if (question9 == "") {
        alert("Oops!!Question 9 is required");
        return false;
    }
    else if (question10 == "") {
        alert("Oops!!Question 10 is required");
        return false;
    }
    else if (name == "") {
        alert("Name field is required");
        return false;
    }

    else {
        if (question1 == "Cascading style sheets") {
            score += 1;
        }
        if (question2 == "style") {
            score += 1;
        }
        if (question3 == "background-color") {
            score += 1;
        }
        if (question4 == "color") {
            score += 1;
        }
        if (question5 == "font-size") {
            score += 1;
        }
        if (question6 == "weight;bold;") {
            score += 1;
        }
        if (question7 == "seperate each selector with a comma") {
            score += 1;
        }
        if (question8 == "static") {
            score += 1;
        }
        if (question9 == "#demo") {
            score += 1;
        }
        if (question10 == "No") {
            score += 1;
        }

    }

    if(score==10){
        alert("Congratulation "+name+ "! You got "+score+" out of 10");
    }
    else if(score>=5){
        alert("Way to go, "+name+"! You got "+score+" out of 10 correctly");
    }
    else{
        alert("Keep trying, "+name+"! You answered "+score+" out of 10 correctly")
    }
}